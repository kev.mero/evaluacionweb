function guardar() {

    // obtener los valores de los inputs
    let cedula = document.getElementById("cedula").value;
    let apellidos = document.getElementById("apellidos").value;
    let nombres = document.getElementById("nombres").value;
    let direccion = document.getElementById("direccion").value;
    let telefono = document.getElementById("telefono").value;

    // valida campos vacios
    if (cedula.length == 0 || 
        apellidos.length == 0 || 
        nombres.length == 0 || 
        direccion.length == 0 ||
        telefono.length == 0  ) {

            alert('campos vacios')
            

        } else {

            // preparar para guardar como objeto
            let datos = {
                'cedula': cedula,
                'apellidos': apellidos,
                'nombres': nombres,
                'direccion': direccion,
                'telefono': telefono
            }

            localStorage.setItem('usuario', JSON.stringify(datos))

        }
    }

    